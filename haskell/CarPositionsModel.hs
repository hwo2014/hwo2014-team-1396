{-# LANGUAGE OverloadedStrings, RecordWildCards, ViewPatterns #-}

module CarPositionsModel where

import Data.Aeson
import Control.Applicative
import Control.Monad
import Data.Maybe
import Data.List
import qualified Data.Map.Strict as M
import qualified Data.Array as A
import RingArray

data Lane = Lane {
    laneDistance :: !Double
  , laneId :: !LaneId
    }
    deriving ( Show, Eq, Read, Ord )

type LaneId = Int
type CarId  = String

-- CarLane

data CarLane = CarLane {
    startLaneIndex :: !LaneId
  , endLaneIndex   :: !LaneId
} deriving (Show, Eq, Read, Ord)

data YourCar = YourCar {
    yourCarName :: !String
  , yourCarColor :: !String
  }
  deriving ( Show, Eq, Read, Ord )

-- Car
data Car = Car {
    carId         :: !CarId
  , angle         :: !Double
  , piecePosition :: !PiecePosition
  , carLength     :: !Double
  , carWidth      :: !Double
} deriving (Show, Eq, Read, Ord )

-- PiecePosition
data PiecePosition = PiecePosition {
    pieceIndex      :: !Int
  , inPieceDistance :: !Double
  , lane            :: !CarLane
  , lap             :: !Int
} deriving (Show, Eq, Read, Ord )

progress :: Track -> PiecePosition -> Double
progress track (PiecePosition {..}) =
    (total_len * fromIntegral lap +
     sum (fmap (flip pieceLength track_lane) pieces_before_this_one) +
     inPieceDistance) /
    total_len
  where
    total_len = totalLength track (fromJust $ M.lookup lane_id (lanes track))
    pieces_before_this_one =
        take pieceIndex $ A.elems $ actualArray (trackPieces track)

    lane_id = endLaneIndex lane
    Just track_lane = M.lookup lane_id (lanes track)

totalLength :: Track -> Lane -> Double
totalLength track lane =
    sum (fmap (flip pieceLength lane) pieces)
  where
    pieces = A.elems $ actualArray (trackPieces track)

pieceLength :: TrackPiece -> Lane -> Double
pieceLength (shape -> Straight len) _ = len
pieceLength (shape -> Curved radius angle) lane =
    actual_radius * (angle / 180.0 * pi)
  where
    actual_radius = radius - dist
    dist = laneDistance lane

data Track = Track {
    cars :: !(M.Map CarId Car)
  , numLaps :: !Int
  , trackId :: String
  , trackPieces :: !(RingArray TrackPiece)
  , lanes :: !(M.Map LaneId Lane)
  }
  deriving ( Show, Eq, Read, Ord) 

data TrackPiece = TrackPiece {
    shape :: !PieceShape
  , switchable :: !Bool
  }
  deriving ( Show, Eq, Read, Ord )

data PieceShape = Straight !Double
                | Curved   !Double !Double
                deriving ( Show, Eq, Read, Ord )

data CarUpdate = CarUpdate
    { carUpdateId :: CarId
    , carUpdateAngle :: !Double
    , carUpdatePos :: !PiecePosition }
    deriving ( Show, Eq, Read, Ord )

instance FromJSON PiecePosition where
  parseJSON (Object v) =
    PiecePosition <$>
    (v .: "pieceIndex")      <*>
    (v .: "inPieceDistance") <*>
    (v .: "lane")            <*>
    (v .: "lap")
  parseJSON _ = empty

instance FromJSON Car where
  parseJSON (Object v) = do
      id <- v .: "id"
      cid <- id .: "color"
      dims <- v .: "dimensions"
      len <- dims .: "length"
      wid <- dims .: "width"
      return $ Car {
          carId = cid
        , angle = 0.0
        , piecePosition = PiecePosition { pieceIndex = 0
                                        , inPieceDistance = 0.0
                                        , lane = CarLane 0 0
                                        , lap  = 0 }
        , carLength = len
        , carWidth  = wid }
  parseJSON _ = empty

instance FromJSON CarUpdate where
  parseJSON (Object v) = do
    id <- v .: "id"
    cid <- id .: "color"
    angle <- v .: "angle"
    pos <- v .: "piecePosition"
    return CarUpdate {
          carUpdateId = cid
        , carUpdateAngle = angle
        , carUpdatePos = pos }
  parseJSON _ = empty

instance FromJSON YourCar where
  parseJSON (Object v) =
    YourCar <$>
    (v .: "name") <*>
    (v .: "color")
  parseJSON _ = empty

instance FromJSON CarLane where
  parseJSON (Object v) =
    CarLane <$>
    (v .: "startLaneIndex") <*>
    (v .: "endLaneIndex")
  parseJSON _ = empty


instance FromJSON TrackPiece where
  parseJSON (Object v) = do
      shape <- (Straight <$> v .: "length") <|>
               (Curved <$> v .: "radius" <*> v .: "angle")
      switchable <- v .:? "switch"
      return TrackPiece { shape = shape
                        , switchable = case switchable of
                                           Nothing -> False
                                           Just x  -> x }
  parseJSON _ = empty

instance FromJSON Lane where
  parseJSON (Object v) = Lane <$>
      v .: "distanceFromCenter" <*>
      v .: "index"
  parseJSON _ = empty

instance FromJSON Track where
  parseJSON (Object v) = do
      tr <- v .: "track"
      session <- v .: "raceSession"
      num_laps <- session .: "laps"
      cars <- mappify <$> v .: "cars"
      pieces <- fromArray . arraify <$> tr .: "pieces"
      tid <- tr .: "id"
      lanes <- mappify2 <$> tr .: "lanes"
      return Track {
          cars = cars
        , numLaps = num_laps
        , trackId = tid
        , trackPieces = pieces
        , lanes = lanes }
    where
      arraify :: [e] -> A.Array Int e
      arraify lst = A.listArray (0, length lst - 1) lst

      mappify :: [Car] -> M.Map CarId Car
      mappify cars = M.fromList (zip (fmap carId cars) cars)

      mappify2 :: [Lane] -> M.Map LaneId Lane
      mappify2 lanes = M.fromList (zip (fmap laneId lanes) lanes)

  parseJSON _ = empty



module MonotonicTime
    ( MonotonicTime
    , getMonotonicTime
    , increaseMonotonicTime )
    where

import Data.IORef
import System.IO.Unsafe

type MonotonicTime = Double  -- unit is seconds

clock :: IORef Double
clock = unsafePerformIO $ newIORef 0.0
{-# NOINLINE clock #-}

getMonotonicTime :: IO MonotonicTime
getMonotonicTime = readIORef clock

increaseMonotonicTime :: IO ()
increaseMonotonicTime = atomicModifyIORef' clock $ \x ->
    ( x+1, () )


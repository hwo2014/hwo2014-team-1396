{-# LANGUAGE RecordWildCards, MultiWayIf #-}

module BayesianRegression
    ( Predictor()
    , MatrixD
    , getPredictorWeights
    , getPredictorVariance
    , bayesianRegression
    , zeroPriorModel
    , addOrder
    , addConstant
    , predict
    , Model(..) )
    where

import qualified Data.Vector as V
import Numeric.Matrix
import Data.Maybe
import Data.Foldable

type MatrixD = Matrix Double

data Model = Model
    { priorWeights :: !(V.Vector Double)
    , priorVariance :: !MatrixD
    , trainingData :: !MatrixD
    , resultData   :: !(V.Vector Double)
    , tau :: !Double
    , noiseSigma :: !Double }
    deriving ( Eq, Show, Read )

data Predictor = Predictor
    { weights :: !MatrixD
    , variance :: !MatrixD
    , noiseSigmaP :: !Double }
    deriving ( Eq, Show, Read )

getPredictorWeights :: Predictor -> V.Vector Double
getPredictorWeights (Predictor {..}) = V.fromList $ col 1 weights

getPredictorVariance :: Predictor -> MatrixD
getPredictorVariance = variance

zeroPriorModel :: MatrixD -> V.Vector Double -> Model
zeroPriorModel training_points training_results =
    Model { priorWeights = V.replicate num_variables 0.0
          , priorVariance = unit $ numCols training_points
          , tau = 1.0
          , noiseSigma = 2
          , trainingData = training_points
          , resultData = training_results }
  where
    num_variables = numCols training_points

addOrder :: MatrixD -> Int -> MatrixD
addOrder training_points order =
    matrix (numRows training_points,
            numCols training_points * order) $ \(row, column) ->
            let j = ((column-1) `mod` numCols training_points) + 1
                o = ((column-1) `div` numCols training_points) + 1
             in at training_points
                   (row, j)**fromIntegral o

addConstant :: MatrixD -> MatrixD
addConstant training_points =
    matrix (numRows training_points
           ,numCols training_points + 1) $ \(row, column) ->
     if | column == 1 -> 1.0
        | otherwise -> at training_points (row, column-1)

matrixify :: V.Vector Double -> MatrixD
matrixify vec = fromList $ fmap return (V.toList vec)

inv' :: MatrixD -> MatrixD
inv' mat = case inv mat of
               Just x -> x
               Nothing -> unit (numCols mat)

bayesianRegression :: Model
                   -> Predictor
bayesianRegression (Model {..}) = Predictor
    { variance = variance
    , weights = left + right
    , noiseSigmaP = noiseSigma }
  where
    variance :: MatrixD
    variance = inv' (inv_variance0 `scale` noiseSigma2 +
                     transpose trainingData * trainingData) `scale`
               noiseSigma2

    noiseSigma2 = noiseSigma**2

    inv_variance0 :: MatrixD
    inv_variance0 = inv' variance0

    variance0 :: MatrixD
    variance0 = priorVariance `scale` (tau**2)

    left :: MatrixD
    left = variance * inv_variance0 * matrixify priorWeights

    right :: MatrixD
    right = variance *
            transpose trainingData *
            matrixify resultData `scale`
            (1/noiseSigma2)

predict :: V.Vector Double -> Predictor -> Double
predict points (Predictor {..}) =
    foldl' folder 0.0 (zip (V.toList points) [0..])
  where
    folder accum (val, index) =
        accum + at weights (index+1, 1) * val


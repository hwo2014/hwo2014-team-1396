{-# LANGUAGE GADTs, GeneralizedNewtypeDeriving #-}

module Control
    ( RaceCarAI()
    , RaceCarAIUpd
    , RaceCarState(..)
    , Updates(..)
    , pedal
    , get
    , newRaceCarAI )
    where

import MonotonicTime
import CarPositionsModel
import Control.Monad.Operational
import Control.Monad.IO.Class
import Control.Concurrent
import Control.Applicative
import Control.Monad
import System.IO
import Data.IORef
import qualified Data.Map.Strict as M
import Data.Foldable

data RaceCarState = RaceCarState
    { trackInfo :: !Track
    , myColor :: !String
    , now :: !Double }

data RaceCarInstruction a where
    Pedal :: Double -> RaceCarInstruction ()
    Get   :: RaceCarInstruction RaceCarState
    DoIO  :: IO a -> RaceCarInstruction a

newtype RaceCarAI a = RaceCarAI (ProgramT RaceCarInstruction IO a)
                      deriving ( Monad, Applicative, Functor )

instance MonadIO RaceCarAI where
    liftIO = RaceCarAI . singleton . DoIO

pedal :: Double -> RaceCarAI ()
pedal = RaceCarAI . singleton . Pedal

get :: RaceCarAI RaceCarState
get = RaceCarAI $ singleton Get

type RaceCarAIUpd = Updates -> IO (Maybe Double)

data Updates =
    Init !Track !MonotonicTime
  | Cars [CarUpdate] !MonotonicTime
  | Color String

newRaceCarAI :: RaceCarAI ()
             -> IO (Updates -> IO (Maybe Double))
newRaceCarAI ai = do
    mvar <- newEmptyMVar
    tid <- forkIO $ aiRun mvar ai
    void $ mkWeakMVar mvar $ killThread tid
    ref <- newIORef Nothing
    color_ref <- newIORef Nothing
    return $ \updates ->
        case updates of
            Color str ->
                atomicModifyIORef' color_ref $ \_ ->
                    ( Just str, Nothing )
            Init track now -> do
                Just color <- readIORef color_ref
                let st = RaceCarState {
                                   trackInfo = track
                                 , myColor = color
                                 , now = now }
                writeIORef ref $ Just st
                new_mvar <- newEmptyMVar
                putMVar mvar $ (st, new_mvar)
                Just <$> takeMVar new_mvar
            Cars updates now -> do
                let folder cars carupdate =
                        M.adjust (\old_car -> old_car
                            { angle = carUpdateAngle carupdate
                            , piecePosition = carUpdatePos carupdate })
                                 (carUpdateId carupdate)
                                 cars
                st <- atomicModifyIORef' ref $ \(Just old) ->
                    let st = old { trackInfo =
                                (trackInfo old)
                                   { cars = foldl' folder
                                                   (cars (trackInfo old))
                                                   updates }
                                    , now = now }
                     in ( Just st, st )

                new_mvar <- newEmptyMVar
                putMVar mvar $ ( st , new_mvar )
                Just <$> takeMVar new_mvar

  where
    updateRound mvar st = do
        new_mvar <- newEmptyMVar
        modifyMVar_ mvar $ return . const (st, new_mvar)
        takeMVar new_mvar

aiRun :: MVar (RaceCarState, MVar Double) -> RaceCarAI () -> IO ()
aiRun mvar (RaceCarAI program) = do
    reports <- takeMVar mvar
    rec reports program
  where
    rec reports program = do
        result <- viewT program
        case result of
            Return () -> hPutStrLn stderr "Warning: AI stopped." >> return ()
            Get :>>= rest -> rec reports (rest $ fst reports)
            Pedal throttle :>>= rest -> do
                putMVar (snd reports) throttle
                new_reports <- takeMVar mvar
                rec new_reports $ rest ()
            DoIO io :>>= rest -> do
                result <- io
                rec reports (rest result)


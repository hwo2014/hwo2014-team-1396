module BizarroAI
    ( bizarroAI )
    where

import Control
import System.Random
import Control.Monad
import Control.Monad.IO.Class

bizarroAI :: RaceCarAI ()
bizarroAI = forever $ do
    r <- liftIO $ randomRIO (0.0, 1.0)
    pedal r


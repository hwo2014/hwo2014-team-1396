{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}

module Main where

import System.Environment (getArgs)
import System.Exit (exitFailure)

import Network(connectTo, PortID(..))
import System.IO(hPutStrLn, hGetLine, hSetBuffering, BufferMode(..), Handle)
import Data.List
import Control.Concurrent
import Control.Monad
import Control.Monad.IO.Class
import Control.Applicative
import qualified Data.ByteString.Lazy.Char8 as L
import Data.Aeson(decode, FromJSON(..), fromJSON, parseJSON, eitherDecode, Value(..), (.:), Result(..))

import CarPositionsModel
import MonotonicTime
import BizarroAI
import System.Timeout

import Control

type ClientMessage = String

joinMessage botname botkey = "{\"msgType\":\"join\",\"data\":{\"name\":\"" ++ botname ++ "\",\"key\":\"" ++ botkey ++ "\"}}"
throttleMessage :: Double -> String
throttleMessage amount = "{\"msgType\":\"throttle\",\"data\":" ++ (show amount) ++ "}"
pingMessage = "{\"msgType\":\"ping\",\"data\":{}}"

connectToServer server port = connectTo server (PortNumber (fromIntegral (read port :: Integer)))

main = do
  args <- getArgs
  case args of
    [server, port, botname, botkey] -> do
      control <- newRaceCarAI bizarroAI
      run server port botname botkey control
    _ -> do
      putStrLn "Usage: hwo2014bot <host> <port> <botname> <botkey>"
      exitFailure

run server port botname botkey control = do
  h <- connectToServer server port
  hSetBuffering h LineBuffering
  hPutStrLn h $ joinMessage botname botkey
  handleMessages h control

handleMessages h control = do
  msg <- hGetLine h
  case decode (L.pack msg) of
    Just json ->
      let decoded = fromJSON json >>= decodeMessage in
      case decoded of
        Success serverMessage -> handleServerMessage h serverMessage control
        Error s -> fail $ "Error decoding message: " ++ s
    Nothing -> do
      fail $ "Error parsing JSON: " ++ (show msg)

data ServerMessage = Join | GameInit Track
                   | CarPositions [CarUpdate] | Unknown String
                   | YourCarMsg YourCar
                   deriving ( Show )

newtype GameInitMsg = GameInitMsg { unwrapTrack :: Track }

handleServerMessage :: Handle -> ServerMessage -> RaceCarAIUpd -> IO ()
handleServerMessage h serverMessage control = do
  responses <- respond serverMessage control
  forM_ responses $ hPutStrLn h
  handleMessages h control

respond :: ServerMessage -> RaceCarAIUpd -> IO [ClientMessage]
respond message control = print message >> case message of
  Join -> do
    putStrLn "Joined"
    return [pingMessage]
  GameInit track -> do
    now <- getMonotonicTime
    increaseMonotonicTime
    Just throttle <- control $ Init track
                               now
    return [throttleMessage throttle]
  CarPositions carPositions -> do
    now <- getMonotonicTime
    increaseMonotonicTime
    maybe_throttle <- timeout (1000000 `div` 90) $
        control $ Cars carPositions now
    print maybe_throttle
    case maybe_throttle of
        Nothing -> return [pingMessage]
        Just (Just throttle) -> return [throttleMessage throttle]
  YourCarMsg ycar -> do
    control $ Color (yourCarColor ycar)
    return [pingMessage]
  Unknown msgType -> do
    putStrLn $ "Unknown message: " ++ msgType
    return [pingMessage]

decodeMessage :: (String, Value) -> Result ServerMessage
decodeMessage (msgType, msgData)
  | msgType == "join" = Success Join
  | msgType == "gameInit" = GameInit . unwrapTrack <$> (fromJSON msgData)
  | msgType == "carPositions" = CarPositions <$> (fromJSON msgData)
  | msgType == "yourCar" = YourCarMsg <$> (fromJSON msgData)
  | otherwise = Success $ Unknown msgType

instance FromJSON GameInitMsg where
  parseJSON (Object v) = do
      r <- v .: "race"
      return $ GameInitMsg r

instance FromJSON a => FromJSON (String, a) where
  parseJSON (Object v) = do
    msgType <- v .: "msgType"
    msgData <- v .: "data"
    return (msgType, msgData)
  parseJSON x          = fail $ "Not an JSON object: " ++ (show x)


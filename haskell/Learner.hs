module Learner
    ( Learner()
    , newLearnerP
    , newLearner
    , teach
    , predict )
    where

import BayesianRegression hiding ( predict )
import qualified BayesianRegression as BR
import Numeric.Matrix
import qualified Data.Vector as V

data Learner = Learner
    { teach   :: !([Double] -> Double -> Learner)
    , predict :: !([Double] -> Double)
    , showing :: String }

instance Show Learner where
    show lrn = showing lrn

newLearnerP :: [Double]
            -> Double
            -> V.Vector Double
            -> MatrixD
            -> Learner
newLearnerP vals result first_prior first_variance =
    teacher first_prior first_variance vals result
  where
    teacher prior variance vals result = Learner
        { teach = teacher (getPredictorWeights new_predictor)
                          (getPredictorVariance new_predictor)
        , predict = \new_vals ->
            BR.predict (V.fromList $ 1:new_vals ++ (fmap (**2) new_vals))
                       new_predictor
        , showing = "Learner: <" ++ show new_predictor ++ ">" }
      where
        new_predictor = bayesianRegression model
        model = Model { tau = 1
                      , noiseSigma = 2
                      , priorWeights = prior
                      , priorVariance = variance
                      , trainingData =
                          addConstant $ addOrder (fromList [vals]) 2
                      , resultData = V.singleton result }


newLearner :: [Double] -> Double -> Learner
newLearner vals result =
    newLearnerP vals result first_prior first_variance
  where
    actual_number_of_vals = num_vals*2+1
    num_vals = length vals

    first_prior = V.fromList $ replicate actual_number_of_vals 0.0
    first_variance = unit actual_number_of_vals

{-
addToTrainingSet :: ([Double], Double)
                 -> [([Double], Double)]
                 -> [([Double], Double)]
addToTrainingSet (vals, result) set =
    if S.size new_set < 20
      then new_set
      else rec 0
  where
    num_vals = length vals
    new_set = (vals, result):set

    rec index
        | index >= num_vals =

    countings index =
        foldl' (\countings (vals, result) ->
                   let val = vals !! index
                    in M.insertWith (+) val 1 countings)
               M.empty
               new_set

    analyze index =
        foldl' (\(mi, ma) (vals, result) ->
                   let val = vals !! index
                    in ( min mi val
                       , max ma val ) )
               (1000000000, -1000000000)
               new_set
-}


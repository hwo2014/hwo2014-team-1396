module RingArray
    ( RingArray()
    , actualArray
    , fromArray
    , (!) )
    where

import qualified Data.Array as A

newtype RingArray a = RingArray { actualArray :: A.Array Int a }
                      deriving ( Eq, Ord, Show, Read )

fromArray :: A.Array Int a -> RingArray a
fromArray arr
    | min /= 0 = error "fromArray: min must be zero."
    | otherwise = RingArray arr
  where
    (min, _) = A.bounds arr

(!) :: RingArray a -> Int -> a
(RingArray arr) ! index = arr A.! (index `mod` max)
  where
    (_, max) = A.bounds arr

